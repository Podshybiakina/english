public with sharing class English {

     public static List<Words__c> resultList = [
            SELECT Word__c, Translation__c
            FROM Words__c
            ORDER BY LastModifiedDate desc
            LIMIT 5000
     ];

    @AuraEnabled
    public static String getTranslationPhrase(String wordToTranslate) {
        String stringForReqwest = wordToTranslate.replace(' ', '+');
        String apiKeyWord = 'trnsl.1.1.20180822T180455Z.8d3e33ae85929bf8.aab00f742d4a3695a59bbc9054229e99faf78855';
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://translate.yandex.net/api/v1.5/tr.json/translate?key=' + apiKeyWord + '&lang=en-ru&text=' + stringForReqwest);
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        String response = res.getBody();
        return response;
    }

    @AuraEnabled
    public static String getTranslationWord(String wordToTranslate) {
        HttpRequest req = new HttpRequest();
        String apiKeyPhrase = 'dict.1.1.20180823T073635Z.d476731fcd416e44.29259a17bf4b8c73d71dafcfbdf13d1224ecca55';
        req.setEndpoint('https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=' + apiKeyPhrase + '&lang=en-ru&text=' + wordToTranslate);
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        String response = res.getBody();
        return response;
    }

    @AuraEnabled
    public static void saveTranslation(String wordToTranslate, List<String> translation) {
        if (String.isBlank(wordToTranslate)) {
            System.debug('There is nothing to translate');
        } else {
            Words__c newWord = new Words__c();
            newWord.Name = wordToTranslate;
            newWord.Word__c = wordToTranslate;
            newWord.Translation__c = String.join(translation, ', ');
            try {
                insert newWord;
            } catch(Exception e) {
                System.debug('An exception occurred: ' + e.getMessage());
            }
        }
    }

    @AuraEnabled
    public static List<Words__c> getSavedWords() {
        return resultList;
    }

    @AuraEnabled
    public static void updateTranslation(String currentWord, String currentTranslation) {
        List<Words__c> wordsToUpdate = [
                SELECT Word__c, Translation__c
                FROM Words__c
                WHERE Word__c =:currentWord
        ];
        for (Words__c word : wordsToUpdate) {
            word.Translation__c = currentTranslation;
        }
        update wordsToUpdate;
    }

}