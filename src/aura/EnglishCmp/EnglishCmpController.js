({
    init : function (component, event, helper) {
        helper.setInitialData(component);
    },

    showInputField : function (component, event, helper) {
        helper.showInputField (component);
    },

    translate : function (component, event, helper) {
        if ((event.keyCode === 13)||(event.which)=== 13) {
            helper.translateTheWord(component);
        }
    },

    clearData : function (component, event, helper) {
        helper.clearData(component);
    },

    rememberTheWord : function (component, translation, helper) {
        helper.validateDataBeforeSaving(component);
    },

    handleSearch : function (component, event, helper) {  //does not work in helper  ????
        component.set('v.isLoading', true);
        var timer = component.get('v.timer');
        clearTimeout(timer);
        var timer = setTimeout(function(){
            helper.searchInSaved(component, event);
            clearTimeout(timer);
            component.set('v.timer', null);
        }, 2000);
        component.set('v.timer', timer);
    },

    highlightDiv : function (component, event, helper) {
        helper.highlightDivOnMouseOver(component, event);
    },

    setInitialBackgroung : function (component, event, helper) {
        helper.highlightDivOnMouseOut(component, event);
    },

    closeModal : function (component, event, helper) {
        helper.closeModal(component);
    },

    openMenu : function (component, event, helper) {
        helper.openMenu(component);
    },

    closeMenu : function (component, event, helper) {
        helper.closeMenu(component);
    },

    selectOptionValue : function (component, event, helper) {
        helper.saveWordWithSelectedTranslations(component);
    },

    repeatWords : function (component, event, helper) {
        helper.setWordToRepeat(component);
    },

    checkTranslation : function (component, event, helper) {
        if ((event.keyCode === 13)||(event.which)=== 13) {
          helper.checkRepitedWord(component);
        }
    },

    handleMenuSelection : function (component, event, helper) {
        helper.handleMenuSelection(component, event);
    },

    editTranslation : function (component, event, helper) {
        helper.editTranslation(component, event);
    },

    forbidEnterPress : function (component, event, helper) {
        helper.forbidEnterPress(component, event);
    },

    skipWord : function (component, event, helper) {
        helper.skipWord(component);
    },

    resetFocus : function (component, event, helper) {
        helper.resetFocus(component);
    },


})