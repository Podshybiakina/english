({
//This logic interacts with front-end or DOM only
//           without calling apex
// It also does not change any app-data

    focusOnField : function (component, fieldToFocus) {
        setTimeout(function(){
            fieldToFocus.focus();
        }, 100);
    },

    parseIfProxy : function(obj) {
        if (obj) {
            return JSON.parse(JSON.stringify(obj));
        }
        return obj;
    },

    resetFocus : function (component) {
        if (!component.get('v.isPlaceholderShown')) {
            component.set('v.isPlaceholderShown', true);
        }
    },

    clearData : function (component) {
        component.set('v.translations', '');
        component.find("wordToTranslate").set("v.value", '');
        component.find("wordToTranslate").set("v.disabled", false);
        component.set('v.isPlaceholderShown', true);
        component.set('v.isRememberButtonShown', false);
        component.find("wordToTranslate").set("v.readonly", false);
    },

    closeModal : function (component) {
        component.set('v.isModalShown', false);
        if (component.get('v.isInputEmpty')) {
            component.set('v.isPlaceholderShown', true);
            component.set('v.isInputEmpty', false);

        } else {
            component.set('v.isPlaceholderShown', false);
            component.set('v.isLoading', false);
        }
    },

    openMenu : function (component) {
        component.set('v.isMenuOpened', true);
    },

    closeMenu : function (component) {
        component.set('v.isMenuOpened', false);
    },

    showInputField : function (component) {
        component.set('v.isPlaceholderShown', false);
        component.set('v.isWordInputShown', true);
        this.focusOnField(component, component.find("wordToTranslate"));
    },

    handleMenuSelection : function (component, event) {
        var currentElementId = event.target.getAttribute("id");

        if (currentElementId === 'fst-item') {
            component.set('v.isMainPage', true);
            component.set('v.isRepetition', false);
            component.set('v.isVocabularyShown', false);
            component.set('v.isPlaceholderShown', true);
            component.set('v.isMenuOpened', false);
            component.set('v.isTranslationSelected', true);
            component.set('v.isVocabularySelected', false);
            component.set('v.isRepetitionSelected', false);

        }
        if (currentElementId === 'scd-item') {
            component.set('v.isMainPage', false);
            component.set('v.isRepetition', false);
            component.set('v.isVocabularyShown', true);
            component.set('v.isPlaceholderShown', false);
            component.set('v.isMenuOpened', false);
            component.set('v.isVocabularySelected', true);
            component.set('v.isTranslationSelected', false);
            component.set('v.isRepetitionSelected', false);

        }
        if (currentElementId === 'thd-item') {
            component.set('v.isMainPage', false);
            component.set('v.isRepetition', true);
            component.set('v.isVocabularyShown', false);
            component.set('v.isPlaceholderShown', false);
            component.set('v.isMenuOpened', false);
            component.set('v.isRepetitionSelected', true);
            component.set('v.isTranslationSelected', false);
            component.set('v.isVocabularySelected', false);
            this.setWordToRepeat(component);
        }
    },

    validateDataBeforeSaving : function (component) {
        var translation = component.get("v.translations");
        var word = this.getStringWithoutWhitespacesToLowerCase(component, component.find("wordToTranslate").get("v.value"));
        if (!translation) {
            component.set('v.modalMessage', 'Translate the word before saving it.');
            return component.set('v.isModalShown', true);
        }
        var savedWords = this.parseIfProxy(component.get('v.savedWords'));
        savedWords.forEach(function(item){
            if (item.Word__c === word) {
                component.set('v.isTheWordSavedTwice', true);
                component.set('v.modalMessage', 'This word was already translated. You just need to repeat it.');
                return component.set('v.isModalShown', true);
            }
        });
        if (!component.get('v.isTheWordSavedTwice')) {
           this.handleCheckboxesInput(component);
        }
    },

    checkIfWordToTranslateEntered : function (component) {
        var wordToTranslate = component.find("wordToTranslate").get("v.value");

        if ((!wordToTranslate)||(wordToTranslate === true)) {
            component.set('v.modalMessage', 'There is nothing to translate here.')
            component.set('v.isModalShown', true);
            return component.set('v.isInputEmpty', true);
        }
    },

    notificateNulltranslations : function (component) {
        component.set('v.modalMessage', 'Oops! I founded nothing. Try again.');
        component.find("wordToTranslate").set("v.readonly", false);
        return component.set('v.isModalShown', true);
    },

    highlightDivOnMouseOver : function (component, event) {
        var currentElementId = event.target.getAttribute("id");
        if (currentElementId) {
            var wordId = 'w' + currentElementId.substring(1);
            var translationId = 't' + currentElementId.substring(1);
            var editId = 'e' + currentElementId.substring(1);
            document.getElementById(wordId).style.backgroundColor =  "rgb(102, 102, 102)";
            document.getElementById(wordId).style.transition =  "0.5s";
            document.getElementById(translationId).style.backgroundColor =  "rgb(102, 102, 102)";
            document.getElementById(translationId).style.transition =  "0.5s";
            document.getElementById(editId).style.backgroundColor =  "rgb(102, 102, 102)";
            document.getElementById(editId).style.transition =  "0.5s";
        }
    },

    highlightDivOnMouseOut : function (component, event) {
        var currentElementId = event.target.getAttribute("id");
        if (currentElementId) {
            var wordId = 'w' + currentElementId.substring(1);
            var translationId = 't' + currentElementId.substring(1);
            var editId = 'e' + currentElementId.substring(1);
            document.getElementById(wordId).style.backgroundColor =  "rgb(33, 33, 33)";
            document.getElementById(wordId).style.transition =  "0.5s";
            document.getElementById(translationId).style.backgroundColor = "rgb(33, 33, 33)";
            document.getElementById(translationId).style.transition =  "0.5s";
            document.getElementById(editId).style.backgroundColor = "rgb(33, 33, 33)";
            document.getElementById(editId).style.transition =  "0.5s";
        }
    },

    getStringWithoutWhitespacesToLowerCase : function (component, stringToHandle) {
        var stringWithoupWhitespacesToLowerCase = stringToHandle.toLowerCase();
        if(stringWithoupWhitespacesToLowerCase.indexOf(' ') >= 0) {
            stringWithoupWhitespacesToLowerCase.split('').forEach(function (letter) {
                if (letter === ' ') {
                    stringWithoupWhitespacesToLowerCase = stringWithoupWhitespacesToLowerCase.slice(0, stringWithoupWhitespacesToLowerCase.indexOf(letter)) + stringWithoupWhitespacesToLowerCase.slice(stringWithoupWhitespacesToLowerCase.indexOf(letter) + 1);
                }
            });
        }
        return stringWithoupWhitespacesToLowerCase;
    },

//    This logic works with apex-controller
//    or changes app-data in any another way


    setInitialData : function (component) {
        component.set('v.isLoading', true);
        var action = component.get('c.getSavedWords');
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.savedWords', response.getReturnValue());
                component.set('v.savedWordsStorage', response.getReturnValue());
                component.set('v.isLoading', false);
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(errors);
            }
        });
        $A.enqueueAction(action);
    },

    translateTheWord : function (component) {
        component.set('v.isLoading', true);
        this.checkIfWordToTranslateEntered(component);
        var wordToTranslate = component.find("wordToTranslate").get("v.value");
        if (/\s/.test(wordToTranslate)) {
            var action = component.get('c.getTranslationPhrase');
            action.setParams({wordToTranslate : wordToTranslate});
            action.setCallback(this, function (response) {
                var state = response.getState();
                component.set('v.isLoading', false);
                if (state === 'SUCCESS') {
                    var responseJSON = response.getReturnValue();
                    var responseObj = JSON.parse(responseJSON);
                    if (responseObj.text.length === 0) {
                        this.notificateNulltranslations(component);
                    } else {
                        component.set('v.translations', responseObj.text);
                        component.set('v.isRememberButtonShown', true);
                        component.find("wordToTranslate").set("v.readonly", true);
                    }
                } else if (state === 'ERROR') {
                    var errors = response.getError();
                    console.log(errors);
                }
            });
            $A.enqueueAction(action);
        } else {
            var action = component.get('c.getTranslationWord');
            action.setParams({wordToTranslate : wordToTranslate});
            action.setCallback(this, function (response) {
                var state = response.getState();
                component.set('v.isLoading', false);
                if (state === 'SUCCESS') {
                    var responseJSON = response.getReturnValue();
                    var responseObj = JSON.parse(responseJSON);
                    if (responseObj.def.length === 0) {
                        this.notificateNulltranslations(component);
                    } else {
                       var allTranslations = [];
                       responseObj.def.forEach(function (definition) {
                           definition.tr.forEach(function (translation) {
                               allTranslations.push(translation.text);
                           });
                       });
                       component.set('v.translations', allTranslations);
                       component.set('v.isRememberButtonShown', true);
                       component.find("wordToTranslate").set("v.readonly", true);
                    }
                } else if (state === 'ERROR') {
                    var errors = response.getError();
                    console.log(errors);
                }
            });
            $A.enqueueAction(action);
        }
    },

    handleCheckboxesInput : function (component) {
        var selectedTranslations = [];
        var checkboxes = component.find("checkbox");
        if(!checkboxes) {   // Find returns zero values when there's no items
            checkboxes = [];
        } else if(!checkboxes.length) { // Find returns a normal object with one item
            checkboxes = [checkboxes];
        }
        checkboxes
        .filter(checkbox => checkbox.get("v.value"))    // Get only checked boxes
        .forEach(checkbox => selectedTranslations.push(checkbox.get("v.label")));   // And get the labels
        if (selectedTranslations.length === 0) {
            component.set('v.modalMessage', 'Select at least one translation to save the word.');
            return component.set('v.isModalShown', true);
        }
        this.saveWordWithSelectedTranslations(component, selectedTranslations);
    },

    saveWordWithSelectedTranslations : function (component, selectedTranslations) {
        var action = component.get('c.saveTranslation');
        action.setParams({
           translation : selectedTranslations,
           wordToTranslate : component.find("wordToTranslate").get("v.value")
        });
        action.setCallback(this, function (response) {
           var state = response.getState();
           if (state === 'SUCCESS') {
               this.setInitialData(component);
               component.set('v.areManyTranslationsShown', false);

           } else if (state === 'ERROR') {
               var errors = response.getError();
               console.log(errors);
           }
        });
        $A.enqueueAction(action);
    },

    searchInSaved : function (component, event) {
        var searchInput = this.getStringWithoutWhitespacesToLowerCase(component, component.find("wordToSearch").get("v.value"));
        var findedValues = [];
        var savedWords = this.parseIfProxy(component.get('v.savedWordsStorage'));
        savedWords.forEach(function (word) {
            if (word.Word__c.includes(searchInput)) {
                findedValues.push(word);
            }
        });
        component.set('v.savedWords', findedValues);
        component.set('v.isLoading', false);
    },

    setWordToRepeat : function(component) {
        component.set('v.isRepetition', true);
        component.set('v.repetitionResult', '');
        var allSavedWords = this.parseIfProxy(component.get('v.savedWordsStorage'));
        var randomIndex = Math.ceil(Math.random()*allSavedWords.length);
        component.set('v.wordToRepeat', allSavedWords[randomIndex].Word__c);
        component.set('v.translationsToCheckRepeatition', allSavedWords[randomIndex].Translation__c);
        this.focusOnField(component, component.find("wordTranslation"));

    },

    checkRepitedWord : function (component) {

        var guessedTransltion = this.getStringWithoutWhitespacesToLowerCase(component, component.find("wordTranslation").get("v.value"));
        var correctTranslations = component.get('v.translationsToCheckRepeatition');

        var isTranslationCorrect = false;
        correctTranslations.split(',').forEach(function (translation) {
            if (translation.toLowerCase() === guessedTransltion) {
                isTranslationCorrect = true;
            }
            if (isTranslationCorrect === true) {
                component.set('v.repetitionResult', 'Correct!');
                component.set('v.isResultShown', true);
            } else {
                component.set('v.repetitionResult', 'Try again!');
                component.set('v.isResultShown', true);
            }
        });
        this.handleRepetitionResult(component);
    },

    handleRepetitionResult : function (component) {
        //component.set('v.isLoading', true);
        var repetitionResult = component.get('v.repetitionResult');
        switch(repetitionResult) {
          case 'Correct!':
          const self = this;
          setTimeout(function(){
              component.find("wordTranslation").set("v.value", '');
              self.setWordToRepeat(component);
             // component.set('v.isLoading', false);
          }, 3000);

            break;
          case 'Try again!':
            component.find("wordTranslation").set("v.value", '');
            setTimeout(function(){
                component.set('v.isResultShown', false);
            }, 3000);
            break;
        }
    },

    skipWord : function (component) {
        this.setWordToRepeat(component);
    },

    editTranslation : function (component, event) {
        var translationId = 't' + event.currentTarget.id.substring(1);
        var wordId = 'w' + event.currentTarget.id.substring(1);
        var currentWord = document.getElementById(wordId).innerHTML;
        this.focusOnField(component, document.getElementById(translationId));
        var buttonText = document.getElementById(event.currentTarget.id).innerHTML.replace(/\s/g,'');;
        if (buttonText === 'edit') {
            this.highlightDivOnMouseOut(component, event);
            document.getElementById(event.currentTarget.id).innerHTML = 'submit';
            document.getElementById(translationId).contentEditable = true;
        } else {
            if (buttonText === 'submit') {
                document.getElementById(event.currentTarget.id).innerHTML = 'edit';
                this.saveEditedTranslation(component, event);
            }
        }
    },

   forbidEnterPress : function (component, event) {
       if ((event.keyCode === 13)||(event.which)=== 13) {
           event.preventDefault();
           this.saveEditedTranslation(component, event);
       }
   },

   saveEditedTranslation : function (component, event) {
       var translationId = 't' + event.currentTarget.id.substring(1);
       var wordId = 'w' + event.currentTarget.id.substring(1);
       var currentWord = document.getElementById(wordId).innerHTML;
       var currentTranslation = document.getElementById(translationId).innerHTML;
       var action = component.get('c.updateTranslation');
       action.setParams({currentWord : currentWord,
            currentTranslation : currentTranslation
       });
       action.setCallback(this, function (response) {
       var state = response.getState();
       component.set('v.isLoading', false);
            if (state === 'SUCCESS') {
                this.setInitialData(component);
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(errors);
            }
       });
       $A.enqueueAction(action);
   },

})